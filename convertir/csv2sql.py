# -*- coding: utf-8 -*-
'''
    re-csv2sql: convertir archivos del Registro Electoral de CSV a SQL
    archivo: csv2sql.py
'''

import sys
import os
import csv
import MySQLdb

class conversor:
    ### tabla electores
    __nombre_tabla_electores = 'electores'
    __columnas_electores = ['nacionalidad', 'cedula', 'primer_apellido', 'segundo_apellido', 'primer_nombre', 'segundo_nombre', 'cod_centro']
    # electores formato MySQL
    __qry_tabla_electores_mysql = 'CREATE TABLE IF NOT EXISTS `{tabla}` ('
    __qry_tabla_electores_mysql += '\n`{nacionalidad}` CHAR NOT NULL ,'
    __qry_tabla_electores_mysql += '\n`{cedula}` INT UNSIGNED NOT NULL ,'
    __qry_tabla_electores_mysql += '\n`{primer_apellido}` MEDIUMTEXT NOT NULL ,'
    __qry_tabla_electores_mysql += '\n`{segundo_apellido}` MEDIUMTEXT NOT NULL ,'
    __qry_tabla_electores_mysql += '\n`{primer_nombre}` MEDIUMTEXT NOT NULL ,'
    __qry_tabla_electores_mysql += '\n`{segundo_nombre}` MEDIUMTEXT NOT NULL ,'
    __qry_tabla_electores_mysql += '\n`{cod_centro}` INT UNSIGNED NOT NULL ,'
    __qry_tabla_electores_mysql += '\nPRIMARY KEY (`{cedula}`) '
    __qry_tabla_electores_mysql += '\n) ENGINE = InnoDB '
    __qry_tabla_electores_mysql += 'DEFAULT CHARACTER SET = utf8 '
    __qry_tabla_electores_mysql += 'COLLATE = utf8_general_ci;\n\n'
    __qry_electores_insert_mysql = 'INSERT INTO `{tabla}` (`{nacionalidad}`, `{cedula}`, `{primer_apellido}`, `{segundo_apellido}`, `{primer_nombre}`, `{segundo_nombre}`, `{cod_centro}`) VALUES \n'
    __qry_electores_insert_values_mysql = '(\'{v1}\', {v2}, \'{v3}\', \'{v4}\', \'{v5}\', \'{v6}\', {v7})'
    
    ### tabla centros
    __nombre_tabla_centros = 'centros'
    __columnas_centros = ['cod_centro', 'tipo', 'cod_estado', 'cod_municipio', 'cod_parroquia', 'nombre_centro', 'direccion_centro', 'centro_nuevo']
    # centros formato MySQL
    __qry_tabla_centros_mysql = 'CREATE TABLE IF NOT EXISTS `{tabla}` ('
    __qry_tabla_centros_mysql += '\n`{cod_centro}` INT UNSIGNED NOT NULL ,'
    __qry_tabla_centros_mysql += '\n`{tipo}` INT UNSIGNED NOT NULL ,'
    __qry_tabla_centros_mysql += '\n`{cod_estado}` INT UNSIGNED NOT NULL ,'
    __qry_tabla_centros_mysql += '\n`{cod_municipio}` INT UNSIGNED NOT NULL ,'
    __qry_tabla_centros_mysql += '\n`{cod_parroquia}` INT UNSIGNED NOT NULL ,'
    __qry_tabla_centros_mysql += '\n`{nombre_centro}` MEDIUMTEXT NOT NULL ,'
    __qry_tabla_centros_mysql += '\n`{direccion_centro}` MEDIUMTEXT NOT NULL ,'
    __qry_tabla_centros_mysql += '\n`{centro_nuevo}` INT UNSIGNED NOT NULL ,'
    __qry_tabla_centros_mysql += '\nPRIMARY KEY (`{cod_centro}`) '
    __qry_tabla_centros_mysql += '\n) ENGINE = InnoDB '
    __qry_tabla_centros_mysql += 'DEFAULT CHARACTER SET = utf8 '
    __qry_tabla_centros_mysql += 'COLLATE = utf8_general_ci;\n\n'
    __qry_centros_insert_mysql = 'INSERT INTO `{tabla}` (`{cod_centro}`, `{tipo}`, `{cod_estado}`, `{cod_municipio}`, `{cod_parroquia}`, `{nombre_centro}`, `{direccion_centro}`, `{centro_nuevo}`) VALUES \n'
    __qry_centros_insert_values_mysql = '({v1}, {v2}, {v3}, {v4}, {v5}, \'{v6}\', \'{v7}\', {v8})'
    
    def verificar(self, archivo_csv, archivo_sql):
        r_csv = os.path.isfile(archivo_csv)
        r_sql = os.path.isfile(archivo_sql)
        
        if not r_csv:
            print archivo_csv + ' no existe.'
        if r_sql:
            print archivo_sql + ' ya existe.'
        
        if ((r_csv) and (not r_sql)):
            return True
        else:
            return None
    
    def convertir(self, archivo_csv, archivo_sql, formato_sql):
        try:
            # lectura del archivo CSV
            with open(archivo_csv, 'r') as f_csv:
                csv.register_dialect('cne', delimiter=';') # estructura del CSV
                datos_csv = csv.reader(f_csv, 'cne') # leer el CSV
                columnas = datos_csv.next() # leer primera linea, nombres de campos
                tabla = self.__crear_tabla(columnas, archivo_sql, formato_sql)
                if tabla == 'Electores':
                    print 'Tabla creada...'
                    try:
                        with open(archivo_sql, 'a+') as f_sql:
                            print 'Llenando datos...'
                            f_sql.write(self.__qry_electores_insert_mysql.format(
                                tabla = self.__nombre_tabla_electores,
                                nacionalidad = self.__columnas_electores[0],
                                cedula = self.__columnas_electores[1],
                                primer_apellido = self.__columnas_electores[2],
                                segundo_apellido = self.__columnas_electores[3],
                                primer_nombre = self.__columnas_electores[4],
                                segundo_nombre = self.__columnas_electores[5],
                                cod_centro = self.__columnas_electores[6]))
                            
                            # llenar VALUES
                            for fila in datos_csv:
                                f_sql.write(self.__qry_electores_insert_values_mysql.format(
                                    v1=MySQLdb.escape_string(fila[0]),
                                    v2=fila[1],
                                    v3=MySQLdb.escape_string(fila[2]),
                                    v4=MySQLdb.escape_string(fila[3]),
                                    v5=MySQLdb.escape_string(fila[4]),
                                    v6=MySQLdb.escape_string(fila[5]),
                                    v7=fila[6]) + ',\n')
                            
                            # eliminar última , y agregar ;
                            f_sql.seek(-2, os.SEEK_END)
                            f_sql.truncate()
                            f_sql.write(';\n')
                            print 'Archivo ' + archivo_sql + ' creado'
                            sys.exit(0)
                    except IOError as e:
                        print e
                elif tabla == 'Centros':
                    print 'Tabla creada...'
                    try:
                        with open(archivo_sql, 'a+') as f_sql:
                            print 'Llenando datos...'
                            f_sql.write(self.__qry_centros_insert_mysql.format(
                                tabla = self.__nombre_tabla_centros,
                                cod_centro = self.__columnas_centros[0],
                                tipo = self.__columnas_centros[1],
                                cod_estado = self.__columnas_centros[2],
                                cod_municipio = self.__columnas_centros[3],
                                cod_parroquia = self.__columnas_centros[4],
                                nombre_centro = self.__columnas_centros[5],
                                direccion_centro = self.__columnas_centros[6],
                                centro_nuevo = self.__columnas_centros[7]))
                            
                            # llenar VALUES
                            for fila in datos_csv:
                                f_sql.write(self.__qry_centros_insert_values_mysql.format(
                                    v1=fila[0],
                                    v2=fila[1],
                                    v3=fila[2],
                                    v4=fila[3],
                                    v5=fila[4],
                                    v6=MySQLdb.escape_string(fila[5]),
                                    v7=MySQLdb.escape_string(fila[6]),
                                    v8=fila[7]) + ',\n')
                            
                            # eliminar última , y agregar ;
                            f_sql.seek(-2, os.SEEK_END)
                            f_sql.truncate()
                            f_sql.write(';\n')
                            print 'Archivo ' + archivo_sql + ' creado'
                            sys.exit(0)
                    except IOError as e:
                        print e
                else:
                    print 'Error: no se pudo crear la tabla'
                    sys.exit(0)
        except IOError as e:
            print e
    
    def __crear_tabla(self, columnas, archivo_sql, formato_sql):
        # diferenciar información de mesas y electores
        if(frozenset(sorted(self.__columnas_centros)) != frozenset(sorted(columnas))):
            # verificar electores
            if(frozenset(sorted(self.__columnas_electores)) == frozenset(sorted(columnas))):
                # ecribir tabla al archivo SQL
                # si es MySQL
                if formato_sql == 'mysql':
                    try:
                        # crear archivo
                        with open(archivo_sql, 'w+') as f_sql:
                            # escribir archivo
                            f_sql.write(self.__qry_tabla_electores_mysql.format(
                                tabla = self.__nombre_tabla_electores,
                                nacionalidad = self.__columnas_electores[0],
                                cedula = self.__columnas_electores[1],
                                primer_apellido = self.__columnas_electores[2],
                                segundo_apellido = self.__columnas_electores[3],
                                primer_nombre = self.__columnas_electores[4],
                                segundo_nombre = self.__columnas_electores[5],
                                cod_centro = self.__columnas_electores[6],))
                    except IOError as e:
                        print e
                # si es Postgresql
                elif formato_sql == 'postgresql':
                    print ('TO-DO')
                # retornar que es tabla de electores
                return 'Electores'
        else:
            # ecribir tabla al archivo SQL
            # si es MySQL
            if formato_sql == 'mysql':
                try:
                    # crear archivo
                    with open(archivo_sql, 'w+') as f_sql:
                        # escribir archivo
                        f_sql.write(self.__qry_tabla_centros_mysql.format(
                            tabla = self.__nombre_tabla_centros,
                            cod_centro = self.__columnas_centros[0],
                            tipo = self.__columnas_centros[1],
                            cod_estado = self.__columnas_centros[2],
                            cod_municipio = self.__columnas_centros[3],
                            cod_parroquia = self.__columnas_centros[4],
                            nombre_centro = self.__columnas_centros[5],
                            direccion_centro = self.__columnas_centros[6],
                            centro_nuevo = self.__columnas_centros[7],))
                except IOError as e:
                    print e
            # si es Postgresql
            elif formato_sql == 'postgresql':
                print ('TO-DO')
            # retornar que es tabla de centros
            return 'Centros'

class ErrorConvertir(Exception):
    def __init__(self, error):
        if error == '':
            self.message = ''
    
    def __str__(self):
        return repr(self.message)
