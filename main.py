#!/usr/bin/env python2
# -*- coding: utf-8 -*-

'''
    re-csv2sql: convertir archivos del Registro Electoral de CSV a SQL
    author: Jesús Jerez <jerezmoreno@gmail.com>
    licencia: BSD
'''

import sys
import argparse
from convertir import csv2sql

def main():
    # argumentos
    parser_args = argparse.ArgumentParser(
      prog='re-csv2sql',
      description='Convertir datos del Registro Electoral de CSV a SQL')
    
    # opciones en argumentos
    parser_args.add_argument('-f', '--csv',
                             required=True,
                             type=str,
                             metavar='entrada.csv',
                             help='archivo de entrada en formato CSV')
    parser_args.add_argument('-o', '--sql',
                             required=True,
                             type=str,
                             metavar='salida.sql',
                             help='archivo de salida en formato SQL')
    parser_args.add_argument('-t',
                             required=True,
                             type=str,
                             choices=['mysql','postgresql'],
                             help='formato especifico de SQL')
    
    # guarda argumentos
    argumentos = vars(parser_args.parse_args())
    
    # verifica que no esté vacío
    if argumentos is not None:
        csv2sql_data = csv2sql.conversor()
        archivos = csv2sql_data.verificar(argumentos['csv'], argumentos['sql'])
        if archivos is not None:
            csv2sql_data.convertir(argumentos['csv'], argumentos['sql'], argumentos['t'])
        else:
            sys.exit(0)
    
    #sys.exit(0)

if __name__ == "__main__":
    sys.exit(main())
